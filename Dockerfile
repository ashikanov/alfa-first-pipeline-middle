FROM nodejscn/node

ENV VERSION 1.0
# ... componets ->  RUN apt install -y package1 package2 & apt update &

WORKDIR /var/wapp

COPY wapp.js .

EXPOSE 8080

ENTRYPOINT node wapp.js # sh -c 'node wapp.js'
# ENTRYPOINT ["node", "wapp.js"]  # exec mode, shell-less
